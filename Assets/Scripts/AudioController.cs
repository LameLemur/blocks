﻿using System;
using UnityEngine;

public class AudioController : MonoBehaviour
{
    public void Awake()
    {
        if (!Values.music)
        {
            gameObject.SetActive(false);
        }
    }
}
