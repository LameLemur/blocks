﻿using UnityEngine;

public class BackgroundHandler : MonoBehaviour
{
    private Transform transform;
    private SpriteRenderer renderer;

    void Start()
    {
        transform = GetComponent<Transform>();
        renderer = GetComponent<SpriteRenderer>();
        SetUpBackground();
    }

    private void SetUpBackground()
    {
        float sizeY = renderer.bounds.size.y;
        Vector3 scale = transform.localScale;
        scale.y = (Values.gridSize) * scale.y / sizeY;
        scale.x = scale.y;
        transform.localScale = scale;
    }
}