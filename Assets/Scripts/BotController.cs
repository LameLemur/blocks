﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = System.Random;

public class BotController : MonoBehaviour
{
    //Components
    private SinglePlayerGameManager manager;

    //Current state
    private int[,] gameGrid;
    private Coord position;

    //New position
    private Coord newPosition;
    private int newRotation;

    private Random rnd;

    private void Start()
    {
        rnd = new Random();
        manager = GetComponent<SinglePlayerGameManager>();
    }

    public void Play(int[,] gameGrid, Coord position)
    {
        this.gameGrid = gameGrid;
        this.position = position;

        CalcRandomPositionAndRotation();
        StartCoroutine(nameof(GoToNewPosition));
    }

    private void CalcRandomPositionAndRotation()
    {
        List<Coord> freePositions = new List<Coord>();
        Coord freePosition;
        for (int i = 0; i < Values.cellCount - 1; i++)
        {
            for (int ii = 0; ii < Values.cellCount - 1; ii++)
            {
                if ((gameGrid[i, ii] + gameGrid[i + 1, ii] + gameGrid[i, ii + 1] + gameGrid[i + 1, ii + 1]) < 2)
                {
                    freePosition = new Coord();
                    freePosition.x = i + 1;
                    freePosition.y = ii + 1;
                    freePositions.Add(freePosition);
                }
            }
        }

        newPosition = freePositions.ToArray()[rnd.Next(0, freePositions.Count())];
        CalcRotation();
    }

    private void CalcRotation()
    {
        if (gameGrid[newPosition.x - 1, newPosition.y - 1] == 1)
        {
            newRotation = 1;
        }
        else if (gameGrid[newPosition.x, newPosition.y - 1] == 1)
        {
            newRotation = 0;
        }
        else if (gameGrid[newPosition.x - 1, newPosition.y] == 1)
        {
            newRotation = 2;
        }
        else if (gameGrid[newPosition.x, newPosition.y] == 1)
        {
            newRotation = 3;
        }
        else
        {
            newRotation = rnd.Next(0, 4);
        }
    }

    IEnumerator GoToNewPosition()
    {
        int stepsY = newPosition.y - position.y;
        int stepsX = newPosition.x - position.x;

        while (stepsY != 0)
        {
            if (stepsY > 0)
            {
                manager.OnMoveCalled(Dir.UP);
                stepsY--;
            }
            else
            {
                manager.OnMoveCalled(Dir.DOWN);
                stepsY++;
            }

            yield return new WaitForSeconds(0.2f);
        }

        while (stepsX != 0)
        {
            if (stepsX > 0)
            {
                manager.OnMoveCalled(Dir.RIGHT);
                stepsX--;
            }
            else
            {
                manager.OnMoveCalled(Dir.LEFT);
                stepsX++;
            }

            yield return new WaitForSeconds(0.2f);
        }

        while (manager.Player.GetComponent<BlockHandler>().indexOfFirst != newRotation)
        {
            manager.Rotate(Dir.RIGHT);
            yield return new WaitForSeconds(0.1f);
        }

        manager.OnPlaceCalled();
    }
}