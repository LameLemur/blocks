﻿using UnityEngine;

public class GameAudioController : AudioController
{
    private GameObject menuAudio;
    
    private void Start()
    {
        menuAudio = GameObject.FindWithTag("MenuAudio");
        menuAudio.SetActive(false);
    }
    
    private void OnDestroy()
    {
        if (Values.music)
        {
            menuAudio.SetActive(true);
        }
    }
}
