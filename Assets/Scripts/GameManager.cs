﻿using UnityEngine;
using UnityEngine.UI;

public struct Coord
{
    public int x;
    public int y;
}

public abstract class GameManager : MonoBehaviour
{
    //Prefabs and player object init
    [SerializeField] public GameObject Player;
    [SerializeField] private GameObject Block;
    protected PlayerHandler pHandler;

    //UI objects init
    [SerializeField] protected GameObject Player1Name;
    [SerializeField] protected GameObject Player2Name;
    protected Text p1Name;
    protected Text p2Name;
    [SerializeField] protected GameObject Player1Score;
    [SerializeField] protected GameObject Player2Score;
    protected Text p1Score;
    protected Text p2Score;

    //Game variable
    protected int[,] gameGrid = new int[Values.cellCount, Values.cellCount];
    public int onTurnPlayerIndex = 0;
    protected Coord currentCoord;
    protected int[] playerScore = {0, 0};

    protected void Init()
    {
        //Component setup
        pHandler = Player.GetComponent<PlayerHandler>();
        p1Score = Player1Score.GetComponent<Text>();
        p2Score = Player2Score.GetComponent<Text>();
        p1Name = Player1Name.GetComponent<Text>();
        p2Name = Player2Name.GetComponent<Text>();

        //Internal logic setup
        ResetGameGrid();
        currentCoord.x = (Values.cellCount + 1) / 2;
        currentCoord.y = (Values.cellCount + 1) / 2;

        //UI setup
        p1Name.text = Values.playerName[0] == "" ? "Player 1" : Values.playerName[0];
        p2Name.text = Values.playerName[1] == "" ? "Player 2" : Values.playerName[1];

        pHandler.SetColor(Values.playerColor[onTurnPlayerIndex]);

        CheckSerializedFields();
    }

    private void CheckSerializedFields() //Makes sure that none of those are empty
    {
        if (Player1Name == null)
            Player1Name = GameObject.Find("Player1Name");
        if (Player2Name == null)
            Player2Name = GameObject.Find("Player2Name");
        if (Player1Score == null)
            Player1Score = GameObject.Find("Player1 score");
        if (Player2Score == null)
            Player2Score = GameObject.Find("Player2 score");
    }

    public void Rotate(Dir dir) //Rotates player
    {
        if (dir == Dir.RIGHT)
            pHandler.RotateRight();
        else
            pHandler.RotateLeft();
    }

    public void OnMoveCalled(Dir dir) //Check if move requests are valid
    {
        switch (dir)
        {
            case Dir.UP:
                if (currentCoord.y + 1 < Values.cellCount)
                {
                    currentCoord.y++;
                    pHandler.Move(dir);
                }

                break;
            case Dir.DOWN:
                if (currentCoord.y - 1 > 0)
                {
                    currentCoord.y--;
                    pHandler.Move(dir);
                }

                break;
            case Dir.LEFT:
                if (currentCoord.x - 1 > 0)
                {
                    currentCoord.x--;
                    pHandler.Move(dir);
                }

                break;
            case Dir.RIGHT:
                if (currentCoord.x + 1 < Values.cellCount)
                {
                    currentCoord.x++;
                    pHandler.Move(dir);
                }

                break;
        }
    }

    public void PlaceBlock() //Places block on the position of player
    {
        switch (Player.GetComponent<BlockHandler>().indexOfFirst)
        {
            case 0:
                gameGrid[currentCoord.x, currentCoord.y] = 1;
                gameGrid[currentCoord.x - 1, currentCoord.y] = 1;
                gameGrid[currentCoord.x - 1, currentCoord.y - 1] = 1;
                break;
            case 1:
                gameGrid[currentCoord.x, currentCoord.y - 1] = 1;
                gameGrid[currentCoord.x - 1, currentCoord.y] = 1;
                gameGrid[currentCoord.x, currentCoord.y] = 1;
                break;
            case 2:
                gameGrid[currentCoord.x, currentCoord.y - 1] = 1;
                gameGrid[currentCoord.x - 1, currentCoord.y - 1] = 1;
                gameGrid[currentCoord.x, currentCoord.y] = 1;
                break;
            case 3:
                gameGrid[currentCoord.x - 1, currentCoord.y] = 1;
                gameGrid[currentCoord.x - 1, currentCoord.y - 1] = 1;
                gameGrid[currentCoord.x, currentCoord.y - 1] = 1;
                break;
        }

        //Creates a block
        Block.GetComponent<BlockHandler>().indexOfFirst = Player.GetComponent<BlockHandler>().indexOfFirst;
        Block.GetComponent<BlockHandler>().color = Values.playerColor[onTurnPlayerIndex];
        Instantiate(Block, Player.transform.position, Quaternion.identity);

        //Changes players color
        onTurnPlayerIndex = (onTurnPlayerIndex + 1) % 2;
        pHandler.SetColor(Values.playerColor[onTurnPlayerIndex]);

        //Checks for game end
        if (IsGameFinished())
        {
            NewGame();
        }
    }

    protected bool IsPlaceFree() //Checks if players position is occupied 
    {
        switch (gameGrid[currentCoord.x, currentCoord.y] + gameGrid[currentCoord.x - 1, currentCoord.y] +
                gameGrid[currentCoord.x, currentCoord.y - 1] + gameGrid[currentCoord.x - 1, currentCoord.y - 1])
        {
            case 0:
                return true;
            case 1:
                switch (Player.GetComponent<BlockHandler>().indexOfFirst)
                {
                    case 0:
                        return gameGrid[currentCoord.x, currentCoord.y - 1] == 1;
                    case 1:
                        return gameGrid[currentCoord.x - 1, currentCoord.y - 1] == 1;
                    case 2:
                        return gameGrid[currentCoord.x - 1, currentCoord.y] == 1;
                    case 3:
                        return gameGrid[currentCoord.x, currentCoord.y] == 1;
                }

                return true;
            default:
                return false;
        }
    }

    private bool IsGameFinished() //Check whether the game is finished
    {
        for (int i = 0; i < Values.cellCount - 1; i++)
        {
            for (int ii = 0; ii < Values.cellCount - 1; ii++)
            {
                if ((gameGrid[i, ii] + gameGrid[i + 1, ii] + gameGrid[i, ii + 1] + gameGrid[i + 1, ii + 1]) < 2)
                    return false;
            }
        }

        return true;
    }

    protected void NewGame() //Reset game
    {
        //Score
        playerScore[(onTurnPlayerIndex + 1) % 2] += 1;
        p1Score.text = playerScore[0].ToString();
        p2Score.text = playerScore[1].ToString();

        //Reset visual
        foreach (GameObject g in GameObject.FindGameObjectsWithTag("Block"))
        {
            Destroy(g);
        }

        //Reset internal
        ResetGameGrid();

        //Reset player
        ResetPlayer();
    }

    private void ResetPlayer() //Resets players position and color
    {
        Player.GetComponent<BlockHandler>().indexOfFirst = 3;
        Player.GetComponent<BlockHandler>().InitRotation();
        pHandler.SetColor(Values.playerColor[onTurnPlayerIndex]);
        Player.GetComponent<Transform>().position = new Vector3(0, 0, 0);
        currentCoord.x = (Values.cellCount + 1) / 2;
        currentCoord.y = (Values.cellCount + 1) / 2;
    }

    private void ResetGameGrid() //Reset the internal grid logic
    {
        for (int i = 0; i < Values.cellCount; i++)
        {
            for (int ii = 0; ii < Values.cellCount; ii++)
            {
                gameGrid[i, ii] = 0;
            }
        }
    }
}