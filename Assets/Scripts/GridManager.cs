﻿using UnityEngine;

public class GridManager : MonoBehaviour
{
    //Gameobjects and components
    [SerializeField] private GameObject innerWall;
    [SerializeField] private GameObject outerWall;
    private Transform parent;

    //Internal variables
    public static float innerWallThickness;

    private void Awake()
    {
        parent = GetComponent<Transform>();
        SetUpBorder();
        SetUpWalls();
    }

    private void SetUpWalls()
    {
        float size = innerWall.GetComponent<Renderer>().bounds.size.x;
        Vector3 scale = innerWall.GetComponent<Transform>().localScale;
        scale.x = Values.gridSize * scale.x / size;
        innerWall.GetComponent<Transform>().localScale = new Vector3(scale.x, Values.innerWallThicknessScale, 1);

        innerWallThickness = innerWall.GetComponent<Renderer>().bounds.size.y;

        for (int i = -Values.cellCount / 2; i < Values.cellCount / 2; i++)
        {
            Instantiate(innerWall,
                new Vector3(0, (Values.GetCellSize() * i + Values.GetCellSize() / (1 + Values.cellCount % 2)), 0),
                Quaternion.identity, parent);
        }

        size = innerWall.GetComponent<Renderer>().bounds.size.y;
        scale = innerWall.GetComponent<Transform>().localScale;
        scale.y = Values.gridSize * scale.y / size;
        innerWall.GetComponent<Transform>().localScale = new Vector3(Values.innerWallThicknessScale, scale.y, 1);

        for (int i = -Values.cellCount / 2; i < Values.cellCount / 2; i++)
        {
            Instantiate(innerWall,
                new Vector3((Values.GetCellSize() * i + Values.GetCellSize() / (1 + Values.cellCount % 2)), 0, 0),
                Quaternion.identity, parent);
        }
    }

    private void SetUpBorder()
    {
        float size = outerWall.GetComponent<Renderer>().bounds.size.y;
        float thickness;
        Vector3 scale = outerWall.GetComponent<Transform>().localScale;
        scale.y = (Values.gridSize + outerWall.GetComponent<Renderer>().bounds.size.y +
                   innerWall.GetComponent<Renderer>().bounds.size.x) * scale.y / size;
        outerWall.GetComponent<Transform>().localScale = new Vector3(Values.outerWallThicknessScale, scale.y, 1);
        thickness = outerWall.GetComponent<Renderer>().bounds.size.y - innerWall.GetComponent<Renderer>().bounds.size.x;
        Instantiate(outerWall,
            new Vector3(
                (Values.GetCellSize() * (-Values.cellCount / 2)) - (Values.cellCount % 2 * Values.GetCellSize() / 2) -
                CalculateOuterWallOffset(thickness), 0, 0), Quaternion.identity, parent);
        Instantiate(outerWall,
            new Vector3(
                (Values.GetCellSize() * (Values.cellCount / 2)) + (Values.cellCount % 2 * Values.GetCellSize() / 2) +
                CalculateOuterWallOffset(thickness), 0, 0), Quaternion.identity, parent);
        outerWall.GetComponent<Transform>().localScale = new Vector3(scale.y, Values.outerWallThicknessScale, 1);
        Instantiate(outerWall,
            new Vector3(0,
                (Values.GetCellSize() * (-Values.cellCount / 2)) - (Values.cellCount % 2 * Values.GetCellSize() / 2) -
                CalculateOuterWallOffset(thickness), 0), Quaternion.identity, parent);
        Instantiate(outerWall,
            new Vector3(0,
                (Values.GetCellSize() * (Values.cellCount / 2)) + (Values.cellCount % 2 * Values.GetCellSize() / 2) +
                CalculateOuterWallOffset(thickness), 0), Quaternion.identity, parent);
    }

    private float CalculateOuterWallOffset(float outerWallSize)
    {
        float res = 0;
        res = (outerWallSize - innerWall.GetComponent<Renderer>().bounds.size.y) / 4;
        return res;
    }
}