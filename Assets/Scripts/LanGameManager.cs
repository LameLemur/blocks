﻿using UnityEngine;

public class LanGameManager : GameManager
{
    //Components
    private LanUIManager uiManager;
    
    private void Awake()
    {
        uiManager = GameObject.Find("NetworkManager").GetComponent<LanUIManager>();
        uiManager.host = Values.gameMode == GameMode.LANHOST;
    }

    private void Start()
    {
        Init();
    }

    public void OnPlaceCalled(GameObject sender)
    {
        if (IsPlaceFree())
        {
            sender.GetComponent<PlayerConnectionController>().RpcPlace();
        }
    }

    public void UpdateNameAndColors()
    {
        p1Name.text = Values.playerName[0] == "" ? "Player 1" : Values.playerName[0];
        p2Name.text = Values.playerName[1] == "" ? "Player 2" : Values.playerName[1];

        pHandler.SetColor(Values.playerColor[onTurnPlayerIndex]);
    }

    private void ScoreReset()
    {
        playerScore[0] = 0;
        playerScore[1] = 0;
        p1Score.text = playerScore[0].ToString();
        p2Score.text = playerScore[1].ToString();
    }

    public void Disconnected()
    {
        NewGame();
        onTurnPlayerIndex = 0;
        ScoreReset();
        UpdateNameAndColors();
        uiManager.Reset();
    }
}