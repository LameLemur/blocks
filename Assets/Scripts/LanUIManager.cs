﻿using System.Collections;
using Mirror;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(NetworkManager))]
public class LanUIManager : NetworkBehaviour
{
    public bool host;
    [SerializeField] private GameObject buttonTextObject;
    private Text buttonText;
    [SerializeField] private GameObject inputFieldTextObject;
    private Text inputFieldText;
    [SerializeField] private GameObject placeHolderTextObject;
    private Text placeHolderText;
    [SerializeField] private GameObject inputFieldObject;

    NetworkManager networkManager;

    void Start()
    {
        networkManager = GetComponent<NetworkManager>();
        buttonText = buttonTextObject.GetComponent<Text>();
        placeHolderText = placeHolderTextObject.GetComponent<Text>();
        inputFieldText = inputFieldTextObject.GetComponent<Text>();
        if (host)
        {
            buttonText.text = "Host a server";
            inputFieldObject.SetActive(false);
        }
        else
        {
            buttonText.text = "Join";
        }
    }

    IEnumerator CheckConnection()
    {
        while (true)
        {
            if (NetworkClient.isConnected)
            {
                buttonText.text = "Disconnect";
                inputFieldObject.SetActive(false);
            }

            if (NetworkServer.active)
            {
                buttonText.text = "Stop";
            }

            yield return new WaitForSeconds(0.5f);
        }
    }

    public void OnButtonClicked()
    {
        switch (buttonText.text)
        {
            case "Host a server":
                networkManager.StartHost();
                break;
            case "Join":
                networkManager.networkAddress = inputFieldText.text == "" ? placeHolderText.text : inputFieldText.text;
                networkManager.StartClient();
                break;
            case "Stop":
                networkManager.StopHost();
                buttonText.text = "Host a server";
                break;
            case "Disconnect":
                networkManager.StopClient();
                buttonText.text = "Join";
                inputFieldObject.SetActive(true);
                break;
        }

        StartCoroutine(nameof(CheckConnection));
    }

    public void Reset()
    {
        if (host && !NetworkServer.active)
        {
            buttonText.text = "Host a server";
        }
        else
        {
            buttonText.text = "Join";
            inputFieldObject.SetActive(true);
        }
    }

    private void OnDestroy()
    {
        networkManager.StopHost();
    }
}