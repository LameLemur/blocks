﻿using UnityEngine;

public class LocalGameManager : GameManager
{
    private void Start()
    {
        Init();
    }

    private void Update()
    {
        //Controls
        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 4])))
        {
            Rotate(Dir.LEFT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 5])))
        {
            Rotate(Dir.RIGHT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 0])))
        {
            OnMoveCalled(Dir.UP);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 2])))
        {
            OnMoveCalled(Dir.DOWN);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 1])))
        {
            OnMoveCalled(Dir.LEFT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 3])))
        {
            OnMoveCalled(Dir.RIGHT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[onTurnPlayerIndex, 6])))
        {
            OnPlaceCalled();
        }
    }

    private void OnPlaceCalled()
    {
        if (IsPlaceFree())
        {
            PlaceBlock();
        }
    }
}