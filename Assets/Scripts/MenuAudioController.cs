﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuAudioController : AudioController
{
    private static MenuAudioController _instance;

    private void Start() 
    {
        if(_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            if(this != _instance)
                Destroy(gameObject);
        }
    }
}
