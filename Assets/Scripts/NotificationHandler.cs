﻿using UnityEngine;
using UnityEngine.UI;

public class NotificationHandler : MonoBehaviour
{
    //Components
    private Text text;

    //Internal variables
    public bool destroyAfterTime = true;
    public string message;

    private void Start()
    {
        text = GetComponentInChildren<Text>();
        if (destroyAfterTime)
            Destroy(gameObject, 1.5f);
        text.text = message;
    }

    public void DestroyNotification()
    {
        Destroy(gameObject);
    }
}