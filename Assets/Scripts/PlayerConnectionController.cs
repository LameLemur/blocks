﻿using System;
using Mirror;
using UnityEngine;

public class PlayerConnectionController : NetworkBehaviour
{
    //Gameobjects and components
    private LanGameManager lanManager;
    private NetworkManager netManager;
    [SerializeField] private GameObject notification;
    private NotificationHandler notHandler;

    //Internal variables
    public bool playerJoinNotificationShown;
    public int playerID = 1;

    private bool sameCellCount = true;

    private void Start()
    {
        sameCellCount = true;

        netManager = GameObject.Find("NetworkManager").GetComponent<NetworkManager>();
        lanManager = GameObject.Find("GameManager").GetComponent<LanGameManager>();
        notHandler = notification.GetComponent<NotificationHandler>();
        playerJoinNotificationShown = false;

        notHandler.destroyAfterTime = true;

        if (isLocalPlayer && isServer)
        {
            notHandler.message = "Server started.";
            Instantiate(notification);
        }

        else if (isLocalPlayer && isClient)
        {
            notHandler.message = "Server joined.";
            Instantiate(notification);
        }

        CmdSyncNameAndColors();

        if (isLocalPlayer && isClient)
        {
            CmdCheckCellCount(Values.cellCount);
        }
    }

    private void Update()
    {
        if (isServer)
        {
            playerID = 0;
        }

        if (!sameCellCount && !isServer)
        {
            netManager.StopClient();
            notHandler.message = "Disconnected due to different cell count.";
            Instantiate(notification);
        }
        
        if (!isLocalPlayer || playerID != lanManager.onTurnPlayerIndex || netManager.numPlayers == 1)
        {
            return;
        }

        if (!playerJoinNotificationShown && isServer)
        {
            playerJoinNotificationShown = true;
            notification.GetComponent<NotificationHandler>().message = "Opponent joined.";
            Instantiate(notification);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 4])))
        {
            CmdRotate(Dir.LEFT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 5])))
        {
            CmdRotate(Dir.RIGHT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 0])))
        {
            CmdOnMoveCalled(Dir.UP);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 2])))
        {
            CmdOnMoveCalled(Dir.DOWN);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 1])))
        {
            CmdOnMoveCalled(Dir.LEFT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 3])))
        {
            CmdOnMoveCalled(Dir.RIGHT);
        }

        if (Input.GetKeyDown((KeyCode) System.Enum.Parse(typeof(KeyCode), Values.keys[0, 6])))
        {
            CmdOnPlaceCalled();
        }
    }

    //Called on server
    [Command]
    private void CmdOnMoveCalled(Dir dir)
    {
        RpcMoveBlock(dir);
    }

    //Called on client
    [ClientRpc]
    private void RpcMoveBlock(Dir dir)
    {
        lanManager.OnMoveCalled(dir);
    }

    //Called on server
    [Command]
    private void CmdRotate(Dir dir)
    {
        RpcRotate(dir);
    }

    //Called on every client
    [ClientRpc]
    private void RpcRotate(Dir dir)
    {
        lanManager.Rotate(dir);
    }

    //Called on server
    [Command]
    private void CmdOnPlaceCalled()
    {
        lanManager.OnPlaceCalled(gameObject);
    }

    //Called on every client
    [ClientRpc]
    public void RpcPlace()
    {
        lanManager.PlaceBlock();
    }

    //Called on server
    [Command]
    private void CmdSyncNameAndColors()
    {
        RpcSyncNameAndColors(Values.playerName, Values.playerColor);
    }

    //Called on every client
    [ClientRpc]
    private void RpcSyncNameAndColors(String[] playerName, BlockColor[] playerColor)
    {
        Values.playerColor = playerColor;
        Values.playerName = playerName;

        lanManager.UpdateNameAndColors();
    }

    //Called on server
    [Command]
    private void CmdCheckCellCount(int count)
    {
        sameCellCount = count == Values.cellCount;
        RpcCheckCellCount(sameCellCount);
    }

    //Called on every client
    [ClientRpc]
    private void RpcCheckCellCount(bool sameCount)
    {
        sameCellCount = sameCount;
    }

    private void OnDestroy()
    {
        lanManager.Disconnected();
        
        foreach (var go in GameObject.FindGameObjectsWithTag("PlayerConnection"))
        {
            go.GetComponent<PlayerConnectionController>().playerJoinNotificationShown = false;
        }

        if (isServer && !isLocalPlayer && sameCellCount)
        {
            notHandler.message = "Opponent disconnected.";
            Instantiate(notification);
        }
        
        if (isServer && !isLocalPlayer && !sameCellCount)
        {
            notHandler.message = "Client wrong cell count.";
            Instantiate(notification);
        }

        if (isLocalPlayer && playerID == 0)
        {
            notHandler.message = "Server stopped.";
            Instantiate(notification);
        }

        if (isLocalPlayer && playerID == 1 && sameCellCount)
        {
            notHandler.message = "Disconnected.";
            Instantiate(notification);
        }

        sameCellCount = true;
    }
}