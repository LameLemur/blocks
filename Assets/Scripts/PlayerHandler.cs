﻿using UnityEngine;

public class PlayerHandler : MonoBehaviour
{
    //Components
    private BlockHandler bHandler;
    private Transform pTranform;
    private SpriteRenderer[] bRenderers;
    private Transform[] bTransform;

    private void Start()
    {
        bHandler = gameObject.GetComponent<BlockHandler>();

        pTranform = gameObject.GetComponent<Transform>();

        bRenderers = new SpriteRenderer[3];
        for (int i = 0; i < 3; i++)
        {
            bRenderers[i] = bHandler.blocks[i].GetComponent<SpriteRenderer>();
        }

        bTransform = new Transform[3];
        for (int i = 0; i < 3; i++)
        {
            bTransform[i] = bHandler.blocks[i].GetComponent<Transform>();
        }
    }

    public void RotateRight()
    {
        for (int i = 0; i < 3; i++)
        {
            bTransform[i].localPosition = bHandler.relativeCord[(i + bHandler.indexOfFirst + 1) % 4];
        }

        bHandler.indexOfFirst = (bHandler.indexOfFirst + 1) % 4;
    }

    public void RotateLeft()
    {
        for (int i = 0; i < 3; i++)
        {
            bTransform[i].localPosition = bHandler.relativeCord[(i + bHandler.indexOfFirst + 3) % 4];
        }

        bHandler.indexOfFirst = (bHandler.indexOfFirst - 1 + 4) % 4;
    }

    public void Move(Dir dir)
    {
        switch (dir)
        {
            case Dir.UP:
                pTranform.Translate(new Vector3(0, Values.GetCellSize(), 0));
                break;
            case Dir.DOWN:
                pTranform.Translate(new Vector3(0, -Values.GetCellSize(), 0));
                break;
            case Dir.LEFT:
                pTranform.Translate(new Vector3(-Values.GetCellSize(), 0, 0));
                break;
            case Dir.RIGHT:
                pTranform.Translate(new Vector3(Values.GetCellSize(), 0, 0));
                break;
        }
    }

    public void SetColor(BlockColor color)
    {
        switch (color)
        {
            case BlockColor.GREEN:
                bRenderers[0].color = Colors.GREEN;
                bRenderers[1].color = Colors.GREEN;
                bRenderers[2].color = Colors.GREEN;
                break;
            case BlockColor.RED:
                bRenderers[0].color = Colors.RED;
                bRenderers[1].color = Colors.RED;
                bRenderers[2].color = Colors.RED;
                break;
            case BlockColor.YELLOW:
                bRenderers[0].color = Colors.YELLOW;
                bRenderers[1].color = Colors.YELLOW;
                bRenderers[2].color = Colors.YELLOW;
                break;
            case BlockColor.BLUE:
                bRenderers[0].color = Colors.BLUE;
                bRenderers[1].color = Colors.BLUE;
                bRenderers[2].color = Colors.BLUE;
                break;
        }
    }
}