﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSelectManager : MonoBehaviour
{
    //Gameobjects and components
    [SerializeField] private GameObject selectOne;
    [SerializeField] private GameObject selectTwo;
    [SerializeField] private GameObject nameOne;
    [SerializeField] private GameObject nameTwo;
    List<string> colors1 = new List<string>() {"Red", "Blue", "Yellow"};
    List<string> colors2 = new List<string>() {"Green", "Blue", "Yellow"};

    //Current choices
    private string firstplayer = "Red";
    private string seondplayer = "Green";

    public void OnFirstPlayerUpdateCalled(int index)
    {
        colors2[colors2.IndexOf(colors1[index])] = firstplayer;
        firstplayer = colors1[index];

        selectTwo.GetComponent<Dropdown>().options.Clear();
        selectTwo.GetComponent<Dropdown>().AddOptions(colors2);
    }

    public void OnSecondPlayerUpdateCalled(int index)
    {
        colors1[colors1.IndexOf(colors2[index])] = seondplayer;
        seondplayer = colors2[index];

        selectOne.GetComponent<Dropdown>().options.Clear();
        selectOne.GetComponent<Dropdown>().AddOptions(colors1);
    }

    public void OnPlayButtonCalled()
    {
        Values.playerColor[0] = ToBlockColor(firstplayer);
        Values.playerColor[1] = ToBlockColor(seondplayer);
        Values.playerName[0] = nameOne.GetComponent<Text>().text;

        if (Values.gameMode == GameMode.SINGLEPLAYER)
        {
            Values.playerName[1] = "Bot";
            GameObject.Find("SceneChanger").GetComponent<SceneChanger>().changescene("single");
        }

        else if (Values.gameMode == GameMode.LOCAL)
        {
            GameObject.Find("SceneChanger").GetComponent<SceneChanger>().changescene("local");
            Values.playerName[1] = nameTwo.GetComponent<Text>().text;
        }

        else
        {
            GameObject.Find("SceneChanger").GetComponent<SceneChanger>().changescene("lan");
            Values.playerName[1] = nameTwo.GetComponent<Text>().text;
        }
    }

    private BlockColor ToBlockColor(string color)
    {
        switch (color)
        {
            case "Green":
                return BlockColor.GREEN;
            case "Red":
                return BlockColor.RED;
            case "Blue":
                return BlockColor.BLUE;
            default:
                return BlockColor.YELLOW;
        }
    }
}