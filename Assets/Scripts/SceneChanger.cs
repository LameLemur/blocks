﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void changescene(string name)
    {
        Values.lastSceneIndex = SceneManager.GetActiveScene().buildIndex;

        switch (name)
        {
            case "menu":
                SceneManager.LoadScene(0);
                break;
            case "single":
                SceneManager.LoadScene(1);
                break;
            case "local":
                SceneManager.LoadScene(2);
                break;
            case "lan":
                SceneManager.LoadScene(3);
                break;
            case "sett":
                SceneManager.LoadScene(4);
                break;
            case "tut":
                SceneManager.LoadScene(5);
                break;
            case "localselect":
                Values.gameMode = GameMode.LOCAL;
                SceneManager.LoadScene(6);
                break;
            case "lanselect":
                Values.gameMode = GameMode.LANHOST;
                SceneManager.LoadScene(7);
                break;
            case "singleselect":
                Values.gameMode = GameMode.SINGLEPLAYER;
                SceneManager.LoadScene(8);
                break;
            case "lanmodeselect":
                Values.gameMode = GameMode.LAN;
                SceneManager.LoadScene(9);
                break;
        }
    }

    public void changescene(int index)
    {
        Values.lastSceneIndex = SceneManager.GetActiveScene().buildIndex;

        SceneManager.LoadScene(index);
    }

    public void changescene()
    {
        if (Values.gameMode == GameMode.LANHOST)
            Values.gameMode = GameMode.LAN;
        SceneManager.LoadScene(Values.lastSceneIndex);
    }

    public void quit()
    {
        Application.Quit();
    }
}