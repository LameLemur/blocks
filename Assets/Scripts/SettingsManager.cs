﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class SettingsManager : MonoBehaviour
{
    //GameObject and components
    [SerializeField] private GameObject[] buttons = new GameObject[14];
    [SerializeField] private GameObject Notification;
    [SerializeField] private GameObject musicButton;
    [SerializeField] private GameObject gridCountDropdown;
    private static GameObject MusicPlayer;
    private NotificationHandler nHandler;
    private Text musicButtonText;
    private Dropdown dropdown;

    private readonly Array keyCodes = Enum.GetValues(typeof(KeyCode));
    private KeyCode currentKeyCode;
    
    //Internal variables
    private int currentKey = -1;

    private void Start()
    {
        if (GameObject.FindWithTag("MenuAudio") != null)
        {
            MusicPlayer = GameObject.FindWithTag("MenuAudio");
        }
        dropdown = gridCountDropdown.GetComponent<Dropdown>();
        musicButtonText = musicButton.GetComponent<Text>();
        
        dropdown.SetValueWithoutNotify(Values.cellCount - 7);
        
        InitKeys();
    }

    private void Update()
    {
        if (currentKey >= 0)
        {
            if (Input.anyKeyDown)
            {
                foreach (KeyCode keyCode in keyCodes)
                {
                    if (Input.GetKey(keyCode))
                    {
                        currentKeyCode = keyCode;
                        break;
                    }
                        
                }
            } 

            string input = currentKeyCode.ToString();

            if (input == "" || IsCurrentKeyTaken(input) || input == "None") return;

            buttons[currentKey].GetComponentInChildren<Text>().text = input;

            if (currentKey > 6)
            {
                Values.keys[1, currentKey - 7] = input;
            }
            else
            {
                Values.keys[0, currentKey] = input;
            }

            currentKey = -1;
            nHandler.DestroyNotification();
        }
    }

    //Called by button press
    public void buttonPressed(int keyIndex)
    {
        if (keyIndex == 14)
        {
            for (int i = 0; i < 7; i++)
            {
                Values.keys[0, i] = Values.defaultKeys[0, i];
                Values.keys[1, i] = Values.defaultKeys[1, i];
            }

            InitKeys();
        }

        else if (keyIndex == 15)
        {
            Values.music = !Values.music;
            musicButtonText.text = Values.music ? "Music: On" : "Music: Off";
            MusicPlayer.SetActive(Values.music);
        }

        else if (keyIndex <= 13)
        {
            if (currentKey == -1)
            {
                Notification.GetComponent<NotificationHandler>().message = "Press key you want to use instead.";
                Notification.GetComponent<NotificationHandler>().destroyAfterTime = false;
                nHandler = Instantiate(Notification).GetComponent<NotificationHandler>();
            }

            currentKey = keyIndex;
        }
    }

    private static bool IsCurrentKeyTaken(string input)
    {
        for (int i = 0; i < 7; i++)
        {
            if (Values.keys[0, i] == input || Values.keys[1, i] == input)
            {
                return true;
            }
        }

        return false;
    }

    private void InitKeys()
    {
        for (int i = 0; i < 7; i++)
        {
            buttons[i].GetComponentInChildren<Text>().text = Values.keys[0, i];
            buttons[i + 7].GetComponentInChildren<Text>().text = Values.keys[1, i];
        }
        
        musicButtonText.text = Values.music ? "Music: On" : "Music: Off";
    }

    public void OnDropdownUpdate(int index)
    {
        Values.cellCount = index + 7;
    }
}