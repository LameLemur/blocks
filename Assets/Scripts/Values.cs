﻿using UnityEngine;

public enum Dir
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}

public enum GameMode
{
    SINGLEPLAYER,
    LOCAL,
    LAN,
    LANHOST
}

public enum BlockColor
{
    RED,
    GREEN,
    BLUE,
    YELLOW
}

public static class Values
{
    //Grid properties
    public static readonly float gridSize = 4.0f;
    public static readonly float innerWallThicknessScale = 0.5f;
    public static readonly float outerWallThicknessScale = 1f;
    public static int cellCount = 9;

    public static float GetCellSize()
    {
        return (gridSize / cellCount);
    }

    //Controls
    public static string[,] keys =
        {{"W", "A", "S", "D", "Q", "E", "Space"}, {"UpArrow", "LeftArrow", "DownArrow", "RightArrow", "N", "M", "K"}};

    public static readonly string[,] defaultKeys =
        {{"W", "A", "S", "D", "Q", "E", "Space"}, {"UpArrow", "LeftArrow", "DownArrow", "RightArrow", "N", "M", "K"}};

    //Player properties
    public static BlockColor[] playerColor = {BlockColor.RED, BlockColor.GREEN};
    public static string[] playerName = {"Wilfréd", "Alfons"};

    public static int lastSceneIndex = 0;
    public static GameMode gameMode = GameMode.LOCAL;

    public static bool music = true;
}

public static class Colors
{
    public static readonly Color RED = ConvertColor(168, 0, 102, 200);
    public static readonly Color GREEN = ConvertColor(168, 229, 48, 200);
    public static readonly Color BLUE = ConvertColor(7, 212, 213, 200);
    public static readonly Color YELLOW = ConvertColor(250, 228, 8, 200);
    public static readonly Color fadedRED = ConvertColor(120, 4, 74, 255);
    public static readonly Color fadedGREEN = ConvertColor(103, 164, 4, 255);
    public static readonly Color fadedBLUE = ConvertColor(0, 134, 137, 255);
    public static readonly Color fadedYELLOW = ConvertColor(234, 187, 4, 255);

    static Color ConvertColor(float r, float g, float b, float a)
    {
        return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
    }
}